(function () {
	"use strict";

	const symbolsPreview = document.querySelector ('[data-js="symbolsPreview"]');
	const svgSprites = document.getElementById ('svgSprites');
	const dropZone = document.querySelector ('[data-js="dropZone"]');



	const loadSVG = function (file) {

		let reader = new FileReader();
		reader.onload = (function (theFile) {
			return function (e) {

				let content = e.target.result;
				let name = escape (theFile.name);
				console.info ('loadSVG', name);

				svgSprites.innerHTML = content;
				parseSprites ();

				return content;
			};
		})(file);

		// reader.readAsDataURL(file);
		reader.readAsText (file);
	};



	const handleFileSelect = function (event) {
		event.stopPropagation();
		event.preventDefault();
		dropZone.classList.remove ('is-active');

		let files = event.dataTransfer.files;
		let svgFile = files[0];

		if (svgFile.type != 'image/svg+xml') return console.error ('SVG only!');
		loadSVG (svgFile);
	}



	const handleDragOver = function (event) {
		event.stopPropagation();
		event.preventDefault();
		event.dataTransfer.dropEffect = 'copy';
		dropZone.classList.add ('is-active');
	}



	const handleDragLeave = function (event) {
		event.stopPropagation();
		event.preventDefault();
		dropZone.classList.remove ('is-active');
	};



	const toDataURL = function (svg, URLencode) {
		URLencode = URLencode || false;
		svg = URLencode ? encodeURIComponent (svg) : svg;
		let dataURL = "data:image/svg+xml,{svgCode}".replace ('{svgCode}', svg);
		return dataURL;
	};



	const parseSprites = function () {

		symbolsPreview.innerHTML = '';
		let sprites = svgSprites.getElementsByTagName ('symbol');

		let buttonDownloadAll = document.createElement ('button');
		buttonDownloadAll.dataset.js = 'buttonDownloadAll';
		buttonDownloadAll.innerText = 'download all';
		buttonDownloadAll.classList.add ('button',  'button--download');

		[].forEach.call (sprites, sprite => {

			let svg = document.createElement ('svg');
			svg.classList.add ('sprite');
			svg.dataset.js = 'sprite';
			svg.setAttribute ('version', '1.1');
			svg.setAttribute ('preserveAspectRatio', 'xMidYMid meet');
			svg.setAttribute ('xmlns', 'http://www.w3.org/2000/svg');
			svg.setAttribute ('viewbox', sprite.getAttribute ('viewBox'));
			svg.setAttribute ('title', sprite.id);
			svg.innerHTML = '<title>' + sprite.id + '</title>';
			svg.innerHTML += sprite.innerHTML;

			let svgLink = document.createElement ('a');
			svgLink.classList.add ('sprite-link');
			svgLink.title = 'download';
			svgLink.href = toDataURL (svg.outerHTML);
			svgLink.target = '_blank-sprite';
			svgLink.innerHTML = svg.outerHTML;

			symbolsPreview.appendChild (svgLink);
		});

		symbolsPreview.appendChild (buttonDownloadAll);

	};



	// https://github.com/NYTimes/svg-crowbar/blob/gh-pages/svg-crowbar-2.js
	const downloadSVG = function (source, filename) {

		filename = !!filename ? filename.replace (/[^a-z0-9]/gi, '-').toLowerCase() : 'svg-sprite';
		source = (new XMLSerializer()).serializeToString (source);
		let url = window.URL.createObjectURL (new Blob([source], {"type" : "text\/xml"}));

		let a = document.createElement ("a");
		document.body.appendChild (a);
		a.setAttribute ("download", filename + ".svg");
		a.setAttribute ("href", url);
		a.style["display"] = "none";
		a.click ();

		setTimeout (function () {
			window.URL.revokeObjectURL (url);
		}, 10);
	};



	// event listeners

	dropZone.addEventListener ('dragover', handleDragOver, false);
	dropZone.addEventListener ('dragleave', handleDragLeave, false);
	dropZone.addEventListener ('drop', handleFileSelect, false);


	symbolsPreview.addEventListener ('click', function (e) {

		// click to download symbol as SVG
		if (e.target && e.target.matches ('[data-js="sprite"]')) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			return downloadSVG (e.target, e.target.getAttribute ('title'));
		}

		// download each symbol as SVG
		if (e.target && e.target.matches ('[data-js="buttonDownloadAll"]')) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();

			let SVGsprites = symbolsPreview.querySelectorAll ('[data-js="sprite"]');

			console.log(SVGsprites);

			[].forEach.call (SVGsprites, sprite => {
				downloadSVG (sprite, sprite.getAttribute ('title'));
			});
		}
	});


})();
